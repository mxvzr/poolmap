var assert = require('assert'),
	R = require('ramda'),
	cp = require('child_process'),
	async = require('async'),
	genPool = require('generic-pool'),
	poolMap = require('../index.js');

const N = require('os').cpus().length;

function getPoolConfig(filename) {
	return {
		create: function(callback) {
			var worker = cp.fork(__dirname + '/' + filename);
			callback(null, worker);
		}
	};
}

var durations = [5, 1, 2, 3, 8];

poolMap(getPoolConfig('worker.js'), N, durations, function(err, result) {
	assert(!err);
	assert(result);
	assert(Array.isArray(result));
	assert.equal(result[0], 5 * 10);
	assert.equal(result[1], 1 * 10);
	assert.equal(result[2], 2 * 10);
	assert.equal(result[3], 3 * 10);
	assert.equal(result[4], 8 * 10);
	console.log('happy case test sucessful');
});

poolMap(getPoolConfig('worker_err.js'), N, durations, function(err, result) {
	assert(err);
	assert(!result);
	console.log('error case test sucessful');
});