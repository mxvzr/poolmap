process.on('message', function(job) {
	var duration = job * 10;
	setTimeout(function() {
		process.send(duration);
	}, duration);
});