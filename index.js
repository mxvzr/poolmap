var cp = require('child_process'),
	async = require('async'),
	genPool = require('generic-pool'),
	R = require('ramda');

module.exports = R.curry(function(poolConf, N, array, callback) {
	var pool = genPool.Pool(R.mergeAll([
		{ destroy: function(worker) { worker.kill() } },
		poolConf,
		{ max: N }
	]));

	async.mapLimit(array, N, function(task, callback) {
		pool.acquire(function(err, worker) {
			if (err) {
				callback(err);
			} else {
				var cleanup = function() {
					worker.removeListener('message', handle_result);
					worker.removeListener('exit', handle_error);
					worker.removeListener('error', handle_error);
					pool.release(worker);
				};
				var handle_result = function(result) {
					cleanup();
					callback(null, result);
				};
				var handle_error = function(err) {
					cleanup();
					callback(err);
				};

				worker.send(task);
				
				worker.on('message', handle_result);
				worker.on('error', handle_error);
				worker.on('exit', handle_error);
			}
		});
	}, function(err, result) {
		pool.drain(function() {
			pool.destroyAllNow();
		});
		if (err) {
			callback(err);
		} else {
			callback(null, result);
		}
	});
});